//
//  AppDelegate.h
//  videoFinder
//
//  Created by afiki on 02.03.13.
//  Copyright (c) 2013 afiki. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
