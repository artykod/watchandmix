//
//  InitialSliderViewController.m
//  videoFinder
//
//  Created by afiki on 22.03.13.
//  Copyright (c) 2013 afiki. All rights reserved.
//

#import "InitialSliderViewController.h"

@interface InitialSliderViewController ()

@end

@implementation InitialSliderViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
