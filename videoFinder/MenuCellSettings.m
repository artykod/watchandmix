//
//  MenuCellSettings.m
//  videoFinder
//
//  Created by afiki on 24.03.13.
//  Copyright (c) 2013 afiki. All rights reserved.
//

#import "MenuCellSettings.h"

@implementation MenuCellSettings

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
